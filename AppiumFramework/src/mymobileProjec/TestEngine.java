/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mymobileProjec;

import mymobileProjec.Appium.AppiumBase;
import mymobileProjec.Appium.Reporting;
import static mymobileProjec.Appium.Reporting.ImagePaths;
import mymobileProjec.TestClasses.Adding;
import mymobileProjec.TestClasses.subtract;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

/**
 *
 * @author smarenene
 */
public class TestEngine extends AppiumBase
{

    FileInputStream fs;
    Sheet sh;
    Workbook kywrd;
    int totalNoOfRows;
    int totalNoOfCols;
    String Keyword = "";
    String ExcelPath;
    static int NumberofTests;
    static String DrivenKey;
    static ArrayList indexRows = new ArrayList();
    static boolean isFailure;

    public TestEngine(String Path) throws FileNotFoundException, IOException, BiffException
    {
        fs = new FileInputStream(Path);
        ExcelPath = Path;
        kywrd = Workbook.getWorkbook(fs);
        sh = kywrd.getSheet("Sheet1");
        totalNoOfRows = sh.getRows();
        totalNoOfCols = sh.getColumns();
    }

    public static int TotNumberOfRuns()
    {
        return NumberofTests;
    }

    public static String DrivenTest()
    {
        return DrivenKey;
    }

    public static void PassRowIndex(ArrayList Index)
    {
        Index.addAll(indexRows);
    }

    public static boolean AlertFailure(boolean PopAlert)// This is a alert method that is used in Test Class to Alert The Test Engine If there is a failed method
    {
        isFailure = PopAlert;
        return isFailure;
    }

    public void RunEngine() throws IOException, BiffException
    {

        mobileInstance.DeviceConfig("emulator-5554", "Android", "Android");//configuring Appium for Emulator

        System.out.println("______________________________STARTING TEST CASES_________________________________");
        System.out.println("______________________________-------------------__________________________________");
        int i = 0;

        while (i < totalNoOfRows)
        {
            if (isFailure == true)
            {
                break;
            }

            if (totalNoOfRows < 0)
            {
                break;
            }
            String KeyWordtemp = sh.getCell(0, i).getContents();

            if (!KeyWordtemp.equalsIgnoreCase("") && !KeyWordtemp.equalsIgnoreCase("end"))
            {
               
                if (!(KeyWordtemp.contains("end")))
                {
                     NumberofTests++;
                    indexRows.add(i);
                    ImagePaths(KeyWordtemp);
                }

                DrivenKey = KeyWordtemp;

                runKeydrivenTestCase(KeyWordtemp);
            }
            i = i + 2;
        }

        System.out.println("-------------------END OF TEST CASES----------------------");
        System.out.println("--------------------SYSTEM SHUTDOWN-----------------------");
        Reporting.WriteToHTMLFile();//writting report to html file
        mobileInstance.quitdriver();

    }

    public void runKeydrivenTestCase(String KeyWord) throws IOException, BiffException
    {
        switch (KeyWord)
        {
            case "Add":
                System.out.println("Running " + KeyWord + " Method. \n");
                Adding plus = new Adding(ExcelPath);
                plus.runTests();
                break;

            case "Subtract":
                System.out.println("Running " + KeyWord + " Method. \n");
                subtract minus = new subtract(ExcelPath);
                minus.runTests();
                break;

        }

    }

}
