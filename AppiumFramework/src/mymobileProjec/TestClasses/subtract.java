/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mymobileProjec.TestClasses;

import mymobileProjec.Appium.AppiumBase;
import mymobileProjec.Appium.ExtractData;
import mymobileProjec.Appium.Reporting;
import mymobileProjec.Xpath.Calculator;
import java.io.IOException;
import java.net.MalformedURLException;
import jxl.read.biff.BiffException;

/**
 *
 * @author smarenene
 */
public class subtract extends AppiumBase
{

    //ExtractData Data;
    ExtractData Data ; 
    public subtract(String Path) throws IOException, BiffException
    {
        Data = new ExtractData(Path);
 
    }

    public boolean runTests() throws MalformedURLException, IOException, BiffException

    {
        if (!AddMethod())
        {
            return false;
        }
        return true;
    }

    public boolean AddMethod() throws MalformedURLException, IOException, BiffException
    {
      
        if (!mobileInstance.clickAndroidElementByXpath(Calculator.Add3(Data.getDataRequired("Num2"))))
        {
            return false;

        }

        if (!mobileInstance.clickAndroidElementByXpath(Calculator.subtractSign()))
        {
            return false;

        }
        if (!mobileInstance.clickAndroidElementByXpath(Calculator.Add2(Data.getDataRequired("Num1"))))
        {
            return false;

        }

        if (!mobileInstance.clickAndroidElementByXpath(Calculator.equalSign()))
        {
            return false;

        }
            Reporting.GenerateReport("Subtract Passed", "Pass", true);
        return true;
    }

}
