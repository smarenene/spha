/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mymobileProjec.Appium;

/**
 *
 * @author smarenene
 */
public class MobileConfig
{

    public enum Configurations
    {

        APPLICATION1("TestApplication\\,com.digitalchemy.calculator.freedecimal-5.1.0-www.APK4Fun.com.apk,Android,emulator-5554");

        private final String url;

        Configurations(String url)
        {
            this.url = url;
        }

        public String url()
        {
            return url;
        }
    }

}
