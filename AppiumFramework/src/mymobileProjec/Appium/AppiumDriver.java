/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mymobileProjec.Appium;

import io.appium.java_client.android.AndroidDriver;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 *
 * @author smarenene
 */
public class AppiumDriver
{

    static AndroidDriver driver;

    public void DeviceConfig(String Devicetype, String Platform, String Device) throws MalformedURLException//setting up appium capabilities
    {
        File appDir = new File("TestApplication\\");
        File app = new File(appDir, "com.digitalchemy.calculator.freedecimal-5.1.0-www.APK4Fun.com.apk");
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("device", Device);

        capabilities.setCapability("deviceName", Devicetype);
        capabilities.setCapability("platformName", Platform);

        capabilities.setCapability("app", app.getAbsolutePath());
        if (!(driver == null))
        {
            driver.resetApp();

        }

        driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);

    }

    public void quitdriver()//this method closes down the appium driver 
    {
        driver.quit();
    }

    //action performed
    public boolean clickAndroidElementByXpath(String xpath)
    {
        try
        {
            driver.findElementByXPath(xpath).click();
            return true;
        }
        catch (Exception e)
        {
            System.err.println("Failed to click Xpath : " + xpath);
        }
        return false;
    }

    public String extractTextUsingXpath(String Xpath, String text)
    {
        String Extracted = driver.findElementByXPath(Xpath).getText();
        if (Extracted.equalsIgnoreCase(text))
        {
            return Extracted;
        }
        System.err.println("Failed to extract by Xpath = " + Xpath);
        return "";

    }

    public boolean waitForElementtoAppear(String xpath, int seconds)
    {

        try
        {
            WebDriverWait wait = new WebDriverWait(driver, seconds);
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath)));
            return true;
        }
        catch (Exception e)
        {
            System.err.println("Could not wait Xpath = " + xpath);
        }
        return false;
    }

}
