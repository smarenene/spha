package mymobileProjec.Appium;

import static mymobileProjec.Appium.AppiumDriver.driver;
import static mymobileProjec.TestEngine.DrivenTest;
import static mymobileProjec.TestEngine.PassRowIndex;
import static mymobileProjec.TestEngine.TotNumberOfRuns;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.Multimap;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Siphamandla
 */
public class Reporting extends AppiumBase
{

    static ArrayList copyiterations;
    public static String userproperty = System.getProperty("user.name");
    public static String s = "";
    static ArrayList TestStep = new ArrayList();
    static ArrayList Status = new ArrayList();
    static ArrayList StatusMessage = new ArrayList();
    static Multimap<String, String> multiMap = LinkedHashMultimap.create();
    static ArrayList TableName = new ArrayList();
    static String FileUrl = "";
    static String ExtractRow = "";
    static int TestCaseNum = 0;
    static String keywords = "";
    static FileInputStream fs;
    boolean checkKeywordPresence;
    int numberOfRws = 0;
    static Sheet sh;
    static Workbook kywrd;
    static int totalNoOfRows;
    static int totalNoOfCols;
    int row = 0;
    ExtractData testData;
    String StoreData = "";
    int checkRuns = 0;
    String[] arrayKEY = new String[1];
    String StringData;
    static String methodRuning = "";
    String current = "";
    String previous = "";
    public static boolean CheckMethoDRunning;
    static String str;
    static String error = "";
    static String Directory = "";
    static int countrun = 0;
    static boolean validateFailMethod = true;
    static boolean FailPass;
    public String subReporting = "";
    boolean tmp;
    static int imageNumber = 0;
    static int i = 0;
    static String mykey = "";
    static String imagePaths = "";
    static String currentTime = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
    static String direct = "MobileTestArtifacts" + currentTime;
    static String imagename = "";
    static boolean MethodFailed = true;
    static int runs = 0;
    static ArrayList RowIndex = new ArrayList();
    static int NumberOfFails = 0;
    static String whereDidItFail = "";

    public Reporting(String Path) throws FileNotFoundException, IOException, BiffException
    {

        this.fs = new FileInputStream(Path);
        this.kywrd = Workbook.getWorkbook(fs);
        this.sh = kywrd.getSheet("Sheet1");
        totalNoOfRows = sh.getRows();
        totalNoOfCols = sh.getColumns();
        this.StringData = Path;

    }

    
       public static void CopyFileToAnotherDirectory(String source, String Destination) throws IOException// this method copies the css file to directory
    {

        try
        {

            File sourcefile = new File(source);
            File distination = new File(Destination);

            FileUtils.copyDirectory(sourcefile, distination);
        }
        catch (Exception e)
        {

            System.err.println("Failed to copy the file to destination.");

        }
    }
    
    public static void exitTest()// this closes the application
    {
        try
        {

            Reporting.WriteToHTMLFile();//writting the report data to html file 
            System.exit(0);
        }
        catch (Exception e)
        {
            System.err.println("[Error] - Failed to exit Test");
        }
    }

    public static void DiplayFromMultMap()
    {

        multiMap = ArrayListMultimap.create();
        // get all the set of keys
        Set<String> keys = multiMap.keySet();

        // iterate through the key set and display key and values
        for (String key : keys)
        {

            Collection<String> collect = multiMap.get(key);
            Iterator<String> iterator = collect.iterator();
            while (iterator.hasNext())
            {
                System.out.println(iterator.next());
            }
        }

    }

//end of CssFile Creator   dding steps to The array List and use it when generating the report
    public static void GenerateReport(String statusMssg, String myStatus, boolean checkClassFail) throws IOException
    {

        imagename = statusMssg;
        TakeScreenShot();
        if (!checkClassFail)
        {
            NumberOfFails++;

            validateFailMethod = checkClassFail;
            multiMap.put(DrivenTest(), statusMssg + "," + myStatus);

        }
        else
        {
            validateFailMethod = checkClassFail;
            multiMap.put(DrivenTest(), statusMssg + "," + myStatus);

        }
    }

    public static void addTableName(String TestName)
    {
        TableName.add(TestName);
    }

//validating if there is a failed method 
    public static boolean ValidateFailedMethod()

    {

        return validateFailMethod;
    }

    public static boolean KeepTrackOfFailPassCase()// keeps track of pass and failed methods on the run

    {
        if (driver == null)

        {
            FailPass = true;
        }
        else

        {
            FailPass = validateFailMethod;
        }

        return FailPass;

    }

    public void KeyWord2(int index)
    {

        mykey = sh.getCell(1, index).getContents();//getting contents of rows 

    }

    public static String RunningMethodImageDirectory()
    {

        return imagePaths;
    }

    public static String CheckMethodStatus()
    {
        String str2 = "";

        return str2;
    }

    public static boolean CheckFailPass(String Keys)// this method checks for passed and failed test for reporting

    {
        Set<String> keys = multiMap.keySet();
        for (String key : keys)
        {
            Collection<String> collect = multiMap.get(key);
            for (Iterator<String> iterator = collect.iterator(); iterator.hasNext();)
            {
                String[] values = iterator.next().split(",");
                if (Keys.equalsIgnoreCase(key))
                {

                    if (values[1].equalsIgnoreCase("Pass"))
                    {
                        return true;
                    }
                    else if (values[1].equalsIgnoreCase("Failed"))
                    {
                        whereDidItFail = values[0];
                    }

                }
            }

        }
        return false;
    }

    public static void CreateReport()
    {

        PassRowIndex(RowIndex);//get the name of method from method name in testengine 
        int helper = 0;
        int rows = 0;
        int Counter = 0;
        int c = 0;
        String TableDataColor = "";
        String currentTime = new SimpleDateFormat("yyyy-MM-dd:HH.mm.ss").format(new Date());
        Set<String> keys = multiMap.keySet();
        int iter = 0;
        String bgColor = "";
        int pass = TotNumberOfRuns();
        s = "<Style>\n"
                + ".toggle-box {\n"
                + "  display: none;\n"
                + "}\n"
                + "\n"
                + ".toggle-box + label {\n"
                + "font-size:100%;"
                + "  cursor: pointer;\n"
                + "  display: block;\n"
                + "  font-weight: bold;\n"
                + "  line-height: 21px;\n"
                + "  margin-bottom: 5px;\n"
                + "}\n"
                + "\n"
                + ".toggle-box + label + div {\n"
                + "  display: none;\n"
                + "  margin-bottom: 10px;\n"
                + "}\n"
                + "\n"
                + ".toggle-box:checked + label + div {\n"
                + "  display: block;\n"
                + "  font-size: 300%;"
                + "}\n"
                + "\n"
                + ".toggle-box + label:before {\n"
                + "  background-color: cornflowerblue;\n"
                + "  -webkit-border-radius: 10px;\n"
                + "  -moz-border-radius: 10px;\n"
                + "  border-radius: 10px;\n"
                + "  color: #FFFFFF;\n"
                + "  content: \"+\";\n"
                + "  display: block;\n"
                + "  float: left;\n"
                + "  font-weight: bold;\n"
                + "  height: 20px;\n"
                + "  line-height: 20px;\n"
                + "  margin-right: 5px;\n"
                + "  text-align: center;\n"
                + "  width: 20px;\n"
                + "}\n"
                + "\n"
                + ".toggle-box:checked + label:before {\n"
                + "  content: \"-\";\n"
                + "}\n"
                + "img{width:80px;height:50px;}"
                + ""
                + "img:active {\n"
                + "   position: fixed; \n"
                + "  top: 0; \n"
                + "  left: 0; \n"
                + "	\n"
                + "  /* Preserve aspet ratio */\n"
                + "  min-width: 25%;\n"
                + "  min-height: 50%;  "
                + "}\n"
                + "* {\n"
                + "    -webkit-transition: all 0.5s ease-in-out;\n"
                + "    -moz-transition: all 0.5s ease-in-out;\n"
                + "    -ms-transition: all 0.5s ease-in-out;\n"
                + "    -o-transition: all 0.5s ease-in-out;\n"
                + "    transition: all 0.5s ease-in-out;\n"
                + "}"
                + "</style>\n"
                + " <link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\">"
                + "<center><h3>Test Execution Date : " + currentTime + "<h3><br> <hr><h3>Test Statistics</h3>"
                + "Total Number Tests      = " + TotNumberOfRuns() + "<br>"
                + "Number of Failed Tests  = " + NumberOfFails + "<br>"
                + "Test Pass Percentage    = " + (((float)(pass - NumberOfFails))/TotNumberOfRuns())*100+ "%<br><h3>Tests</h3>"
                + "<hr></center>"
                + "<body background=\"../../BackgroundWallpaper/it.jpg\">"
                + "<div id=\"page\">\n"
                + "        \n"
                + "\n";
        int z = 1;

        
        //*This method create a report 
                
        
        for (String key : keys)//creatng the reporting for tests
        {
            // int index = Integer.parseInt(RowIndex.get(i).toString());
            String mystatus = "";
            Collection<String> collect = multiMap.get(key);
            imagePaths = key;//Create directory with the name of folder ;
            //ImagePaths();
            //  int size = Iterators.size(iterator);

            if (CheckFailPass(key))
            {
                mystatus = " <i> <font color= 'Green'> Test Passed </font></i>";

            }
            else
            {
                mystatus = " <i><font color= 'Red'> Test Failed  <small>" + whereDidItFail + "</small></font> </i>";
            }
            String[] Run = key.split("_");

            s = s.concat("<input class=\"toggle-box\" id='identifier-" + i + "' type=\"checkbox\">\n"
                    + "<label for='identifier-" + i + "'> Running method is :" + key + " Test Status :->" + mystatus + "</label>\n <div>\n"
                    + "\n"
                    + " <table style=\"width:90%\">\n"
                    + "  <tr>\n"
                    + "    <td bgcolor=''>Test Results</td>\n"
                    + "    <td bgcolor=''>Image Captured</td>\n"
                    + "    <td>Status Message</td>\n"
                    + "  </tr>\n");

            String temporar = "";
            //s = s.concat("<tr>\n");

            for (Iterator<String> iterator = collect.iterator(); iterator.hasNext();)
            {

                String[] values = iterator.next().split(",");
                try
                {
                    if (values[1].equalsIgnoreCase("Pass"))//checks if the its pass the change the color of the text to lime 
                    {
                        bgColor = "Lime";

                        s = s.concat("<tr bgcolor= '" + bgColor + "'>"
                                + "<td>" + values[0] + " </td>"
                                + "<td> <img class = 'enlarge' src='" + imagePaths + "//" + iter + "-" + values[0] + ".jpg'/></td>"
                                + "<td>" + values[1] + "</td></tr>\n");
                    }

                    if (values[1].equalsIgnoreCase("Failed")) // checks if failed and change the table row to Pink
                    {
                        bgColor = "Pink";

                        s = s.concat("<tr bgcolor= '" + bgColor + "'>"
                                + "<td>" + values[0] + " </td>"
                                + "<td> <img class='enlarge' src='" + imagePaths + "//" + iter + "-" + values[0] + ".jpg'/></td>"
                                + "<td>" + values[1] + "</td></tr>\n");
                    }
                    iter++;

                }
                catch (Exception e)
                {
                    break;
                }

            }

   
            s = s.concat(
                    "\n"
                    + "</table> \n"
                    + "\n");

            s = s.concat("</div>\n <hr color='Red'>");
            i++;
            helper = 0;
            rows++;
            c = c + 2;

        }

        s = s.concat("\n"
                + "\n"
                + "\n"
                + "</div> </body>");

    }

    public void myDirectory()
    {
        createDirectory();
    }

    public static String getReportPath()//returning report Location
    {

        return Directory;

    }

    //creating Directory for 
    public static void createDirectory()
    {//creating directory for report Information

        String currentTime = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
        String direct = "MobileTestingArtifacts" + currentTime;
        Directory = direct;
        Path dir = Paths.get("htmlReport\\" + direct);

        if (!Files.exists(dir))
        {
            try
            {
                Files.createDirectories(dir);

            }
            catch (IOException e)
            {
            }
        }
    }

    public static void ImagePaths(String Path)
    {//creating directory for report Information

        Directory = direct;

        Path images = Paths.get("htmlReport\\" + direct + "\\" + Path);

        try
        {
            Files.createDirectories(images);

        }
        catch (IOException e)
        {
            
            System.err.println("Failed to create image directory.");
        }

    }

    public static void WriteToHTMLFile() throws IOException //writing to Html fie 
    {

             
        CopyFileToAnotherDirectory("cssstyle\\", "htmlReport\\" + direct);
       

        CreateReport();//creating html table with report Information
        FileWriter fWriter = null;
        FileWriter Fcss = null;
        FileWriter xcelFile = null;

        BufferedWriter writer = null;
        File f = new File("htmlReport\\" + Directory + "\\fileName.html");
        try
        {
            //Creating Html File
            fWriter = new FileWriter("htmlReport\\" + Directory + "\\fileName.html");
            writer = new BufferedWriter(fWriter);
            writer.write(s);
            writer.newLine(); //this is not actually needed for html files - can make your code more readable though 
            writer.close(); //make sure you close the writer object 
        }
        catch (Exception e)
        {
            //catch any exceptions here
        }

    }

//taking Screenshots
    public static void TakeScreenShot() throws IOException
    {
        try
        {

            System.out.println("Capturing screen shot........wait ");
            File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(scrFile, new File("htmlReport\\" + Directory + "\\" + DrivenTest() + "\\" + (imageNumber + "-" + imagename) + ".jpg"));
            imageNumber++;
            System.out.println("Done");
        }
        catch (IOException e)
        {

            System.out.println("[Error] -  Failed to Take ScreenShot");
        }

    }
}
