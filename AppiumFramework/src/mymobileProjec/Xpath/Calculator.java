/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mymobileProjec.Xpath;

import mymobileProjec.Appium.AppiumBase;

/**
 *
 * @author smarenene
 */
public class Calculator 
{

    public static String Add2(String txt)
    {
        return "//android.widget.ImageView[@resource-id='com.digitalchemy.calculator.freedecimal:id/n"+txt+"']";
    }

    public static String Add3(String txt)
    {
        return "//android.widget.ImageView[@resource-id='com.digitalchemy.calculator.freedecimal:id/n"+txt+"']";
    }

    public static String AddingSign()
    {
        return "//android.widget.ImageView[@resource-id='com.digitalchemy.calculator.freedecimal:id/add']";
    }

    public static String subtractSign()
    {
        return "//android.widget.ImageView[@resource-id='com.digitalchemy.calculator.freedecimal:id/subtract']";
    }

    
      public static String equalSign()
    {
        return "//android.widget.ImageView[@resource-id='com.digitalchemy.calculator.freedecimal:id/equals']";
    }
    
}
