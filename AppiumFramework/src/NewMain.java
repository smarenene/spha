/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author smarenene
 */
public class NewMain
{

    public enum WhoisRIR
    {

        ARIN("whois.arin.net"),
        RIPE("whois.ripe.net"),
        APNIC("whois.apnic.net"),
        AFRINIC("whois.afrinic.net"),
        LACNIC("whois.lacnic.net"),
        JPNIC("whois.nic.ad.jp"),
        KRNIC("whois.nic.or.kr"),
        CNNIC("ipwhois.cnnic.cn"),
        UNKNOWN("");

        private String url;

        WhoisRIR(String url)
        {
            this.url = url;
        }

        public String url()
        {
            return url;
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        System.out.println(WhoisRIR.ARIN.url());
    }

}
